package ru.andrey.task1rangeip;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Andrey O.
 */
public class IpAddress {
    
    //Константа, содержащая шаблон для проверки корректности введенного ip-адреса
    private static final String IPADDRESS_PATTERN = 
            "^(0|[1-9]\\d?|1\\d?\\d?|2[0-4]\\d?|25[0-5])\\." + //0 или 1-99 или ... или 200-249 или 250-255
             "(0|[1-9]\\d?|1\\d?\\d?|2[0-4]\\d?|25[0-5])\\." +
             "(0|[1-9]\\d?|1\\d?\\d?|2[0-4]\\d?|25[0-5])\\." +
             "(0|[1-9]\\d?|1\\d?\\d?|2[0-4]\\d?|25[0-5])$";
    
    /**
     * Метод преобразования строкового занчения ip-адреса в целое число
     * 
     * @param ip - строковое значение ip-адреса
     * @return результат проверки корректности введенного ip-адреса
     */
    public static boolean validateIP(String ip){
        Pattern pattern = Pattern.compile(IPADDRESS_PATTERN);
        Matcher matcher = pattern.matcher(ip);
        return matcher.matches();
    }
    
    /**
     * Метод преобразования строкового занчения ip-адреса в целое число
     * 
     * @param ip - строковое значение ip-адреса
     * @return целочисленное представление ip-адреса
     */
    public static long convertStringIpToNumber(String ip) {
        Pattern pattern = Pattern.compile("\\.");
        String[] octets = pattern.split(ip);
        long ipNumber = 0;
        for (int i=0; i<4; i++) {
            ipNumber += (long) Integer.parseInt(octets[i])*(int)Math.pow(256, 3-i);
        }        
        return ipNumber;
    }
    
    /**
     * Метод преобразования строкового занчения ip-адреса в целое число
     * 
     * @param ip - целочисленное значение ip-адреса
     * @return строковое представление ip-адреса вида 'w.x.y.z'
     */
    public static String convertNumberIpToString(long ip) {
        long mask = 255;
        int [] octets = new int[4];
        
        for (int i = 0; i<4; i++) {
            octets[i] = (int) (ip & mask);
            ip >>= 8;                  
        }
        
        return octets[3]+"."+octets[2]+"."+octets[1]+"."+octets[0];
    }
}
