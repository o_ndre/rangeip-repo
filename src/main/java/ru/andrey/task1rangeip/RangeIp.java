package ru.andrey.task1rangeip;

import java.util.Scanner;

/**
 *
 * @author Andrey O.
 */
public class RangeIp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
                
        System.out.println("Введите начальный ip-адрес для диапазона:");
        String ipString1 = in.nextLine();
        while (!IpAddress.validateIP(ipString1)) {
            System.out.println("Не корректно введен ip-адрес. Попробуйте снова:");
            ipString1 = in.nextLine();
        }
        long firstIp = IpAddress.convertStringIpToNumber(ipString1);
        
        System.out.println("Введите конечный ip-адрес для диапазона:");
        String ipString2 = in.nextLine();
        while (!IpAddress.validateIP(ipString2)) {
            System.out.println("Не корректно введен ip-адрес. Попробуйте снова:");
            ipString2 = in.nextLine();
        }
        long lastIp = IpAddress.convertStringIpToNumber(ipString2);
        
        //Если ip-адреса были введены не в прямом порядке, то меняем значения для корректного задания диапазона
        if (firstIp > lastIp) {
            firstIp += lastIp;
            lastIp = firstIp-lastIp;
            firstIp -=lastIp;
        }
        
        System.out.println("Будет выполнен вывод ip-адресов из диапазона " + IpAddress.convertNumberIpToString(firstIp) +" - " + IpAddress.convertNumberIpToString(lastIp));
        for (long i=firstIp+1; i<lastIp; i++){
            System.out.println(IpAddress.convertNumberIpToString(i));
        }
    }
   
}
