package ru.andrey.task1rangeip;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Andrey O.
 */
public class IpAddressTest {
    
    /**
     * Test of validateIP method, of class IpAddress.
     */
    @Test
    public void validateIP() {
        
        assertEquals(false, IpAddress.validateIP(""));
        assertEquals(false, IpAddress.validateIP("1234.0.0.0"));
        assertEquals(false, IpAddress.validateIP("-.0.0.0"));
        assertEquals(false, IpAddress.validateIP(".0.0.0"));
        assertEquals(false, IpAddress.validateIP("0..0.0"));
        assertEquals(false, IpAddress.validateIP("A.0.0.0"));
        assertEquals(false, IpAddress.validateIP("256.0.0.0"));
        assertEquals(false, IpAddress.validateIP("01.0.0.0"));
        assertEquals(false, IpAddress.validateIP("0.0.0."));
        assertEquals(false, IpAddress.validateIP("0:0.0.0"));
        assertEquals(false, IpAddress.validateIP("0/0.0.0"));
        assertEquals(false, IpAddress.validateIP("0|0.0.0"));
        
        assertEquals(true, IpAddress.validateIP("0.0.0.0"));
        assertEquals(true, IpAddress.validateIP("1.0.0.0"));
        assertEquals(true, IpAddress.validateIP("9.0.0.0"));
        assertEquals(true, IpAddress.validateIP("10.0.0.0"));
        assertEquals(true, IpAddress.validateIP("20.0.0.0"));
        assertEquals(true, IpAddress.validateIP("99.0.0.0"));
        assertEquals(true, IpAddress.validateIP("199.0.0.0"));
        assertEquals(true, IpAddress.validateIP("255.0.0.0"));        
    }

    /**
     * Test of convertStringIpToNumber method, of class IpAddress.
     */
    @Test
    public void convertStringIpToNumber() {
        
        assertTrue(IpAddress.convertStringIpToNumber("0.0.0.0") == 0L);
        assertTrue(IpAddress.convertStringIpToNumber("10.53.35.141") == 171254669L);
        assertTrue(IpAddress.convertStringIpToNumber("255.255.255.255") == 4294967295L);
    }

    /**
     * Test of convertNumberIpToString method, of class IpAddress.
     */
    @Test
    public void ConvertNumberIpToString() {
        
        assertEquals("0.0.0.0", IpAddress.convertNumberIpToString(0L));
        assertEquals("153.204.37.12", IpAddress.convertNumberIpToString(2580292876L));
        assertEquals("255.255.255.255", IpAddress.convertNumberIpToString(4294967295L));
    }
    
}
